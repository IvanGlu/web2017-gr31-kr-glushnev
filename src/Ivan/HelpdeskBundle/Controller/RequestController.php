<?php

namespace Ivan\HelpdeskBundle\Controller;

use Ivan\HelpdeskBundle\Entity\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class RequestController extends Controller
{
    /**
     * Lists all request entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $requests = $em->getRepository('IvanHelpdeskBundle:Request')->findAll();

        return $this->render('request/index.html.twig', array(
            'requests' => $requests,
        ));
    }

    /**
     * Creates a new request entity.
     *
     */
    public function newAction(\Symfony\Component\HttpFoundation\Request $request)
    {
        $req = new Request();
        $form = $this->createForm('Ivan\HelpdeskBundle\Form\RequestForm', $req);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($req);
            $em->flush();

            return $this->redirectToRoute('request_show', array('id' => $req->getId()));
        }

        return $this->render('request/new.html.twig', array(
            'req' => $req,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a request entity.
     *
     */
    public function showAction(Request $req)
    {
        $deleteForm = $this->createDeleteForm($req);

        return $this->render('request/show.html.twig', array(
            'req' => $req,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to delete a request entity.
     *
     * @param bid $bid The bid entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Request $req)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('request_delete', array('id' => $req->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Displays a form to edit an existing request entity.
     *
     */
    public function editAction(\Symfony\Component\HttpFoundation\Request $request, Request $req)
    {
        $deleteForm = $this->createDeleteForm($req);
        $editForm = $this->createForm('Ivan\HelpdeskBundle\Form\RequestForm', $req);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

//            return $this->redirectToRoute('request_edit', array('id' => $bid->getId()));
        }

        return $this->render('request/edit.html.twig', array(
            'req' => $req,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a request entity.
     *
     */
    public function deleteAction(\Symfony\Component\HttpFoundation\Request $request, Request $req)
    {
        $form = $this->createDeleteForm($req);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($req);
            $em->flush();
        }

        return $this->redirectToRoute('request_index');
    }
}
