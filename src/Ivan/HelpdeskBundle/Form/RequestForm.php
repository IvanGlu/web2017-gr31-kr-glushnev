<?php

namespace Ivan\HelpdeskBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class RequestForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('text')
                ->add('category', ChoiceType::class, array(
                    'choices' => array(
                        'Category 1' => 'Category 1',
                        'Category 2' => 'Category 2',
                        'Category 3' => 'Category 3',
                    )))
                ->add('date');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ivan\HelpdeskBundle\Entity\Request'
        ));
    }

    public function getBlockPrefix()
    {
        return 'ivan_helpdesk_bundle_request_form';
    }
}
